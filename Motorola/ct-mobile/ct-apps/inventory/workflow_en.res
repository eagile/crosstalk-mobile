﻿// General font definition
button.font_family=Arial
label.font_family=Arial

// General buttons
btMnExit.label=Exit
btMnBack.label=Back
btMnNext.label=Next
btMnUpdate.label=Update
btMnSend.label=Send

// Screens
f1_lbTitle.value=Select Inventory Location
f1_lbPullDown1Header.value=RTLS Areas:

f2_lbTitle.value=Area:
f2_lbTotalHeader.value=Total
f2_lbFoundHeader.value=Found
f2_lbMissingHeader.value=Missing
f2_lbNewHeader.value=New

// Infos
info_001.value=Select Area first.
info_002.value=
info_003.value=
info_004.value=Server check...
info_005.value=Server not available.
info_006.value=Download Objects...
info_007.value=RTLS Areas found:
info_008.value=No Objects found.
info_009.value=Objects ready for Inventory.
info_010.value=Download Areas...
info_011.value=Selected Area:
info_012.value=Sending data...
info_013.value=Data send successful.
info_014.value=Server Error.


// RFID/Barcode messages
rfid_001.value=RFID Reader on.
rfid_002.value=Barcode Scanner on.
rfid_003.value=Searching...