BarcodeScanner.open('');

rfidReader.unique = true;
rfidReader.pollPeriod = 500;
rfidReader.txPower = parseInt(Device.Properties("txPowerMax"));
rfidReader.open('');


//--------global variables for complete app---------------//

var g_beeperVolume = parseInt(Device.Properties("beeperVolume"));

var g_txPowerMin = parseInt(Device.Properties("txPowerMin")); // 500
var g_txPowerMax = parseInt(Device.Properties("txPowerMax")); // 3000

var g_appCenter = new AppCenter();
g_appCenter.url = Device.Properties("appCenterURL");

var g_connect = false;
var g_kbOpen = false;

var g_dlAreas = new DataList();
g_dlAreas.headers = ["name","id"];

var g_areaId = "";
var g_areaName = "";

var g_dlObjects = new DataList();
g_dlObjects.headers = ["epc","found"];

var g_dlNewItems = new DataList();
g_dlNewItems.headers = ["epc"];

var g_total = 0;
var g_missing = 0;
var g_found = 0;
var g_new = 0;

var g_readPermition = false;
var g_err = 0;

var handheldID = Device.Properties("deviceID");

//-------------------------- Common functions ------------------------------//

var SEVERITY_ERROR = "ERROR";
var SEVERITY_WARN = "WARN";
var SEVERITY_INFO = "INFO";
var SEVERITY_BLINK = "BLINK";

function resetInfo() {
	state.infoLabel.value = "";
}

function setInfo(text, severity) {
	state.infoLabel.hidden = false;
	if (severity == "WARN") {
		state.infoLabel.color = "blue";
		state.infoLabel.text_decoration = "none";
	}
	if (severity == "ERROR") {
		state.infoLabel.color = "red";
		state.infoLabel.text_decoration = "none";
	}
	if (severity == "INFO") {
		state.infoLabel.color = "green";
		state.infoLabel.text_decoration = "none";
	}
	if (severity == "BLINK") {
		state.infoLabel.color = "blue";
		state.infoLabel.text_decoration = "blink";
	}
	state.infoLabel.value = text;
}

function exit() {
	Window.exit();
}

function checkServerAvailable() { 
	with (state) {
			
		var msg = ResourceBundle.get('info_004'); // Server check...
		setInfo(msg, "BLINK");
		
		var settings = g_appCenter.queryElements("type=com:nofilis:app:settings;$select=id");
		// java.lang.System.out.println("settings: " + settings);
		
		var status = "" + settings;
		
		if (status != "[]" && status != "null") { // Server online
			g_connect = true;
			
			resetInfo();
				
		} else { // Server offline
			g_connect = false;
			
			var msg = ResourceBundle.get('info_005'); // Server not available.
			setInfo(msg, "ERROR");
			Beeper.beepN(3, 200, 200, 4000, g_beeperVolume);
		}
	}
}

function errorBeeper() {
	Beeper.beepN(3, 200, 200, 4000, g_beeperVolume);
}

function goodBeeper() {
	Beeper.beepN(2, 200, 200, 3000, g_beeperVolume);	
}

function singleGoodBeeper() {
	Beeper.beepN(1, 200, 200, 3000, g_beeperVolume);	
}

function shortGoodBeeper() {
	Beeper.beepN(1, 100, 100, 3000, g_beeperVolume);	
}


//-----------------------------------------------------------------------------------//
//
//                State F1 - Location                                                //
//
//-----------------------------------------------------------------------------------//

function initF1() {
	with (state) {
		resetInfo();
		btMnUpdate.focus();
	}	
}

function f1_updateData() {
	with (state) {
		
		checkServerAvailable();
		
		if (!g_connect) { // Server not reachable
			return;
		}		
		
		var msg = ResourceBundle.get('info_010'); // Download Areas...
		setInfo(msg, "BLINK");
		
		g_dlAreas.removeAll();		
		
		var url = Device.Properties("appCenterURL") + "element/query?type.id=com:nofilis:rtls:area&$contentType=text/csv&$select=name,id";
		
		var loader = new CSVLoader();
		loader.delimiter = ";";
		var areas = loader.loadUrl(url, true);
		
		// java.lang.System.out.println("areas: " + areas);
		
		if (areas != null || areas != undefined) {
			if (areas.rowCount() > 0) {
				
				for (var m=0; m<areas.rowCount(); m++) { 
					
					var addArea = g_dlAreas.addBean();
					addArea.name = "" + areas[m].name;	
					addArea.id = "" + areas[m].id;					
				}
				
				// java.lang.System.out.println("g_dlAreas: " + g_dlAreas);
				
				areas.removeAll();				
			}
		}
		
		f1_ml1Create();
		
		var msg = ResourceBundle.get('info_007'); // Objects downloaded:
		setInfo(msg + " " + g_dlAreas.rowCount(), "WARN");			
	}	
}

function f1_ml1Create() {
	with (state) {
		
		// create dropdown1
		f1_ml1.removeAllItems(); 
		
		g_dlAreas.sort("name asc");
		
		for (var i=0; i<g_dlAreas.rowCount(); i++ ) { 
			f1_ml1.appendItems(g_dlAreas[i].name,g_dlAreas[i].id);
		}		
	}
}

function f1_ml1Selected() { 
	with (state) {
				
		g_areaName = "" + f1_ml1.selectedItem;
		g_areaId = "" + f1_ml1.selectedItem2;	
				
		var msg = ResourceBundle.get('info_011'); // Selected:
		setInfo(msg + " " + g_areaName, "WARN");
	}	
}

function f1_checkArea() {
	with (state) {
				
		if (g_areaId.equals("")) {
			
			var msg = ResourceBundle.get('info_001'); // Select Area first.
			setInfo(msg, "ERROR");
			
			Beeper.beepN(3, 200, 200, 4000, g_beeperVolume);
			return false;		
		}
		
		
		f1_downloadObjects();
		
		Engine.pause(1000);
		return true; // target="f2"/>
	}	
}

function f1_downloadObjects() {
	with (state) {
		
		checkServerAvailable();
		
		if (!g_connect) { // Server not reachable
			return;
		}		
		
		var msg = ResourceBundle.get('info_006'); // Download Objects...
		setInfo(msg, "BLINK");
		
		g_dlObjects.removeAll();		
		
		var url = Device.Properties("appCenterURL") + "element/query?type.id=com:nofilis:rtls:tag&area=" + g_areaId + "&$contentType=text/csv&$select=id";
		
		//"element/query?type.id=com:nofilis:app:tracking:type:object&area=" + g_areaId + "&$contentType=text/csv&$select=id";
														//"element/query?type.id=com:nofilis:rtls:tag&area=" + g_areaId + "&$contentType=text/csv&$select=id"
		
		var loader = new CSVLoader();
		loader.delimiter = ";";
		var obj = loader.loadUrl(url, true);
		
		// java.lang.System.out.println("obj: " + obj);
		// java.lang.System.out.println("obj: " + obj.rowCount());
		
		if (obj != null || obj != undefined) {
			if (obj.rowCount() > 0) {
				
				for (var m=0; m<obj.rowCount(); m++) { 
					
					var addObj = g_dlObjects.addBean();
					addObj.epc = "" + obj[m].id;
					addObj.found = "0";				
				}
				
				// java.lang.System.out.println("g_dlObjects: " + g_dlObjects);
				
				obj.removeAll();	
				
				g_total = g_dlObjects.rowCount();
			}
		}
		
		if (g_total < 1) { // list empty -> ERR
			var msg = ResourceBundle.get('info_008'); // No Objects found.
			setInfo(msg, "ERROR");
			Beeper.beepN(3, 200, 200, 4000, g_beeperVolume);	
			
		} else {
			
			var msg = ResourceBundle.get('info_009'); // Objects ready for Inventory.
			setInfo(g_total + " " + msg, "INFO");
		}			
	}	
}


//-----------------------------------------------------------------------------------//
//
//                State F2 - Inventory                                               //
//
//-----------------------------------------------------------------------------------//

function initF2() {
	with (state) {
		
		resetInfo();
		
		f2_lbTitle.value = ResourceBundle.get('f2_lbTitle') + " " + g_areaName;
		
		g_missing = g_total - g_found;
		g_new = g_dlNewItems.rowCount();
		
		f2_lbTotal.value = g_total.toString();	
		f2_lbFound.value = g_found.toString();
		f2_lbMissing.value = g_missing.toString();
		f2_lbNew.value = g_new.toString();	
		
		btMnSend.hidden = true;
	}	
}


function f2_startMultiTag() {
			
	var msg = ResourceBundle.get('rfid_001'); // RFID Reader on.
	setInfo(msg, "BLINK");
	
	g_readPermition = true;
	
	rfidReader.txPower = g_txPowerMax;
	rfidReader.startRead();		
}

function f2_stopMultiTag() {
		
	g_readPermition = false;
		
	rfidReader.stopRead();
	
	resetInfo();
	
	if (g_new < 1) { // no found -> hidden
		
		state.btMnSend.hidden = true;
		
	} else {
		
		state.btMnSend.hidden = false;
	}		
}

function f2_getTags() {
	with (state) {
		// java.lang.System.out.println("getTags() called");
		
		if (!g_readPermition) {
			return;
		}
		
				
		var t = "";
		var findTag = false;
		var findNewTag = false;
		var idx = 0;
		var checkFound = "";
	
		var tags = rfidReader.getCurrentTagsCollection();
					
		for (var i=0; i<tags.length; i++) {
			t = "" + tags[i];
			
			if (t.equals("null")) {
				continue;
			}
						
			findTag = g_dlObjects.search("epc", t);
			
			if (!findTag) { // no item from inventory list detected
				
				findNewTag = g_dlNewItems.search("epc", t); 
				
				if (findNewTag) { // already in new list
					continue;
					
				} else {
					
					var addTag = g_dlNewItems.addBean();
					addTag.epc = "" + tags[i];	
				}											
							
			} else { // item belongs to inventory list
				
				// check duplicate
				idx = g_dlObjects.searchIndex("epc", t);
				checkFound = "" + g_dlObjects[idx].found;
				
				if (checkFound.equals("1")) { // skip duplicate
					continue;
				}
				
				// update this inventory tag data
				g_dlObjects[idx].found = "1";
				g_found++;			
			}
			
			// missing ones stay as found="0"
			
			shortGoodBeeper();			
		}
		
		// display result
		g_missing = g_total - g_found;
		g_new = g_dlNewItems.rowCount();
		
		f2_lbFound.value = g_found.toString();
		f2_lbMissing.value = g_missing.toString();
		f2_lbNew.value = g_new.toString();	
	}
}

function f2_checkReadBtn() { //for the reader testbuttons 
	with (state) {
		
		if (!btTest2.checked) { // unchecked
			
			// Engine.pause(1000);
			f2_readBtnUp();	
			
		} else {
			
			// Engine.pause(1000);
			f2_readBtnDown();	
		}
	}
}

function f2_readBtnDown() {
	// java.lang.System.out.println("down -> started");
	
	// start MultiRead
	f2_startMultiTag();	
}

function f2_readBtnUp() {
	// java.lang.System.out.println("up -> stopped");
	
	// stop Multiread
	f2_stopMultiTag();
}

//nordic
function f2_sendData() {
	with (state) {

		var t = new String(Engine.currentTime());
		
		var checkEpc = "";
		var checkState = "0";

		for (var m=0; m<g_dlNewItems.rowCount(); m++) { // ["epc","found"];
						
			checkEpc = "" + g_dlNewItems[m].epc;	
			sendEvent(checkEpc,t);		
				
			
		}
			
			// delete
		g_dlObjects.removeAll();
		g_dlNewItems.removeAll();

		g_total = 0;
		g_missing = 0;
		g_found = 0;
			
		Engine.pause(1000);			
				
	}	
}

/*
function f2_sendData() {
	with (state) {
		
		checkServerAvailable();
		
		if (!g_connect) { // Server not reachable
			return;
		}		
		
		btMnSend.hidden = true;
			
		g_err = 0;
		var t = new String(Engine.currentTime());
		
		var msg = ResourceBundle.get('info_012'); // Sending data...
		setInfo(msg, "BLINK");
		
		var checkEpc = "";
		var checkState = "0";

		for (var m=0; m<g_dlObjects.rowCount(); m++) { // ["epc","found"];
						
			checkState = "" + g_dlObjects[m].found;	
			
			if (checkState.equals("1")) { // // found
				
				checkEpc = "" + g_dlObjects[m].epc;	
				sendEvent(checkEpc,t);				
			} 
		}
		
		
		if (g_err > 0) {
			
			var msg = ResourceBundle.get('info_014'); // Server error.
			setInfo(msg, "ERROR");			
			errorBeeper();	
			
			btMnSend.hidden = false;
						
		} else {
			
			var msg = ResourceBundle.get('info_013'); // Data send successful.
			setInfo(msg, "INFO");	
			goodBeeper();
			
			
			// delete
			g_dlObjects.removeAll();
			g_dlNewItems.removeAll();

			g_total = 0;
			g_missing = 0;
			g_found = 0;
			g_new = 0;
			
			Engine.pause(2000);
			Engine.sendEvent("back.pressed");			
		}		
	}	
}
*/
function sendEvent(epc,t) {
	with (state) {
		
		markerIdHandheld = "HANDHELD_" + g_areaId.toUpperCase();
		
		var e = new Event("com:nofilis:crosstalk:event:tag-observation");
		e.locationId = "" + markerIdHandheld;
		e.timestamp = "" + t;
		e.objectId = "" + epc;
		  	    
	    // java.lang.System.out.println("Event: " + e);
	    
		var result = g_appCenter.sendEvent(e);
		
		if (g_appCenter.statusCode != 200) {
			g_err++;			
		}
		else {
			setInfo(g_dlNewItems.rowCount() + " Tags Sent", "INFO");
		}
		
	}	
}