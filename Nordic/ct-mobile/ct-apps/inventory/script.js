BarcodeScanner.open('');
HardwareButton.open('');

rfidReader.unique = true;
rfidReader.pollPeriod = 600;
rfidReader.txPower = parseInt(Device.Properties("txPowerRead"));
rfidReader.activeAntennas = Device.Properties("activeAntennas");
rfidReader.open('');

var handheldID = Device.Properties("deviceID");

sender = new AsyncHttp();
sender.retryPeriod = 60000;
sender.cacheSize = 100;
sender.cacheFolder = Device.Properties("cacheFolder");
sender.useCache = true;

//--------global variables for complete app---------------//
var g_beeperVolume = parseInt(Device.Properties("beeperVolume"));
var g_barcodeReadTime = parseInt(Device.Properties("barcodeReadTime")); 
var tags12 = null;
var g_location = "";
var e = new Event();
var missingList = null;
var totalItems = 0;
var missingItems = 0;
var foundItems = 0;
var newItems = 0;
var barcodeScanned = true;
var newTags = new TagsCollection();
var epcValues = "";
var totalTagsFound = 0;
var itemsNewFound = 0;
var listItem = "";

var g_dlAreas = new DataList();
g_dlAreas.headers = ["name","id"];

var g_areaId = "";
var g_areaName = "";

g_dlNewItems = new DataList();
g_dlNewItems.headers = ["epc", "found"];

var g_dlObjects = new DataList();
g_dlObjects.headers = ["epc","found"];

var g_dlData = new DataList();
g_dlData.headers = ["Serial","EPCID","Location","Type","Description","Status"];

var g_dlInventory = new DataList();
g_dlInventory.headers = ["Serial","EPCID","Location","Type","Description","Status"];

var g_appCenter = new AppCenter();
g_appCenter.url = Device.Properties("appCenterURL");

var g_total = 0;
var g_missing = 0;
var g_found = 0;
var g_new = 0;
var g_current = 0;

var rowItemsTags = [];

//Other
var g_red = "#FE2E2E";
var g_green = "#01DF3A";// brighter: "#00FF33";
var g_blue = "#3366FF";




// ---------- Common Functions ---------- //

function resetInfo() {
	with (state) {		
	
		infoLabel.value = "";
		infoLabel.hidden = true;
	}
}

function setInfo(text, severity) {
	with (state) {
		
		infoLabel.hidden = false;
		
		if (severity == "WARN") {
			infoLabel.color = g_blue;
			infoLabel.text_decoration = "none";
		}
		if (severity == "ERROR") {
			infoLabel.color = g_red;
			infoLabel.text_decoration = "none";
		}
		if (severity == "INFO") {
			infoLabel.color = g_green;
			infoLabel.text_decoration = "none";
		}
		if (severity == "BLINK") {
			//infoLabel.color = g_blue;
			infoLabel.text_decoration = "blink";
			
		}
		
		infoLabel.value = text;
	}
}

function errorBeeper() {
	Beeper.beepN(3, 200, 200, 4000, g_beeperVolume);
}

function goodBeeper() {
	Beeper.beepN(2, 200, 200, 3000, g_beeperVolume);	
}

function singleGoodBeeper() {
	Beeper.beepN(1, 200, 200, 3000, g_beeperVolume);	
}

function shortGoodBeeper() {
	Beeper.beepN(1, 100, 100, 3000, g_beeperVolume);	
}

function toggleKeyboard() {
	with (state) {
		
		if (g_kbOpen) { // close it
			g_kbOpen = false;
			CEService.sipShowIM(false);
					
		} else { // open it
			g_kbOpen = true;
			CEService.sipShowIM(true);	
		}		
	}
}

function exit() {

	if (g_current > 0) {
		var x = Window.alert("Device contains reads. \nAre you sure you'd like to exit?", "Alert", "yes cancel");

		if (x.equals("#yes")) {
			Window.exit();

		}
	} 
	else {
		Window.exit();

	}
}

function changePower() {
	var x = Window.alert("Change Power", "Power Settings", "yes ok cancel");
	
	
}

//---------- Location.xml ---------- //

function initLocation() {
	with (state) {
		
		resetInfo();
		
		if (g_dlData.rowCount() > 0) {
			
			// prepare menulist
			// prepare subLocation dropdown and datalist
			var checkLoc = "";
			var lastLoc = "";
			mlLocation.removeAllItems();
			
			// headline in dropdown
			if (g_dlData.rowCount() > 0) {
				mlLocation.appendItems("Please Select Location", "null");
			}
				
			for (var i=0; i<g_dlData.rowCount(); i++) { 
				
				checkLoc = "" + g_dlData[i].Location;
							
				if (!checkLoc.equals(lastLoc)) {
					// new subLoc
					mlLocation.appendItems(checkLoc, checkLoc);
					lastLoc = "" + checkLoc;		
				}			
			}
		}	
	}
}

function initF3() {
	with (state) {


	}

}

function initF2() {
	with (state) {
		
		resetInfo();

		g_missing = g_total - g_found;
		g_new = g_dlNewItems.rowCount();
		
		itemsNew.value = g_new.toString()
		itemsNewFound.value =g_found.toString();
        itemsMissing.value = g_dlObjects.rowCount();
		itemsTotalCurrent.value = "0";
		currentLocationId.value = g_areaId.toString(); 
		currentPower.value = rfidReader.txPower.toString();
	}	
}

function prepareInventCheck() {
	with (state) {

		g_areaId = mlLocation.selectedItem2;

		setInfo(g_areaId, "INFO");	
		//Engine.pause(1000);
		
		var s = new Storage();
		var file = s.removeFile("\\Flash\\ct-mobile\\ct-data\\Tags.csv");
		
		//remove file
		s = new Storage();
		file = s.removeFile("\\Flash\\ct-mobile\\ct-data\\areas.csv");
		//send query
		var urlTags = g_appCenter.url + "element/query?type.id=com:nofilis:rtls:tag&area=" + g_areaId + "&$contentType=text/csv&$select=id";
		
		//http://192.168.23.79:8080/nofilis-app-center/dispatcher/element/query?type.id=com:nofilis:rtls:tag&area=Production&$contentType=text/csv&$select=id
		
		//create file
		var fileTags = new Storage().createFile("\\Flash\\ct-mobile\\ct-data\\Tags.csv");
		var httpT = new XmlHttp();
		var y = httpT.loadFile(urlTags, fileTags);
		//f1_downloadObjects();
		
		loadData3("file:" + Device.Properties("inventdata"));
		
		return true;		
	}	
}


// ---------- LoadData.xml ---------- //

function loadFile() {
	
	var s = new Storage();
	var file = s.removeFile("\\Flash\\ct-mobile\\ct-data\\areas.csv");

	getRTLSlocations();
	loadData2("file:" + Device.Properties("inventoryList"));
	//Engine.pause(1000);
}

function loadURL() {
	
	var dlInventoryTmp = null;
	newTags = new TagsCollection();
	
	var objects = g_appCenter.queryElements("type=com:nofilis:app:tracking:type:object");

	if (objects == null) {
		setInfo("Data could not be loaded.", "ERROR");
		return false;
	}
	
	g_dlInventory.removeAll();
			
	for (var i=0; i<objects.rowCount(); i++ ) {
		
		var addData = g_dlInventory.addBean();			
		addData.Serial = "" + objects[i].comment; 
		addData.EPCID = "" + objects[i].id;
		addData.Location = "" + objects[i][6];
		addData.Type = "" + objects[i][17];				
		addData.Description = "" + objects[i].name;
		addData.Status = "0";		
	}
	
	if (g_dlInventory.rowCount() < 1) {
		setInfo("Data could not be loaded.", "ERROR");
		return false;
	}
	
	// java.lang.System.out.println("g_dlInventory: " + g_dlInventory);
	
	totalItems = g_dlInventory.rowCount();
	missingItems = g_dlInventory.rowCount();
	foundItems = 0;
	newItems = 0;	
	setInfo("Inventory items loaded: " + g_dlInventory.rowCount(), "INFO");	
}

function getRTLSlocations() {
	
	var urlRTLS = g_appCenter.url + "/%20element/query?type.id=com:nofilis:rtls:area&$contentType=text/csv&$select=id";
	var file = new Storage().createFile("\\Flash\\ct-mobile\\ct-data\\areas.csv");
	var http = new XmlHttp();
	var x = http.loadFile(urlRTLS, file);
}   

function loadData3(source) {
	with (state) {		
		
		// Load data
		var dlInventoryTmp = null;
		newTags = new TagsCollection();
		
		var loader = new CSVLoader();
		loader.delimiter = ';';
		dlInventoryTmp = loader.loadUrl(source, true);
		
		if (dlInventoryTmp == null) {
			setInfo("File not found.", "ERROR");
			return false;
		}
	
		if (dlInventoryTmp.rowCount() < 1) {
			setInfo("Data could not be loaded.", "ERROR");
			return false;
		}
		
		g_dlObjects.removeAll();
		
		for (var i=0; i<dlInventoryTmp.rowCount(); i++ ) {
			
			var addData = g_dlObjects.addBean();	
			
			 addData.epc = "" + dlInventoryTmp[i][0];
			 addData.found = "0";
	
		}
		
		g_total = g_dlObjects.rowCount();
		
		 setInfo("Tags Found: " + g_total.toString(), "INFO");
		 Engine.pause(1000);
		
	}
}
 

function loadData(source) { // g_dlInventory
	with (state) {		
		
		// Load data
		var dlInventoryTmp = null;
		newTags = new TagsCollection();
		
		var loader = new CSVLoader();
		loader.delimiter = ';';
		dlInventoryTmp = loader.loadUrl(source, true);
		// java.lang.System.out.println("source: " + source);
		
		if (dlInventoryTmp == null) {
			setInfo("File not found.", "ERROR");
			return false;
		}
	
		if (dlInventoryTmp.rowCount() < 1) {
			setInfo("Data could not be loaded.", "ERROR");
			return false;
		}
		
		g_dlInventory.removeAll();
		
		for (var i=0; i<dlInventoryTmp.rowCount(); i++ ) {
			
			var addData = g_dlInventory.addBean();				
			addData.Serial = "" + dlInventoryTmp[i][0]; 
			addData.EPCID = "" + dlInventoryTmp[i][1];
			addData.Location = "" + dlInventoryTmp[i][2];
			addData.Type = "" + dlInventoryTmp[i][3];				
			addData.Description = "" + dlInventoryTmp[i][4];
			addData.Status = "0";		
		}
	
		totalItems = g_dlInventory.rowCount();
		missingItems = g_dlInventory.rowCount();
		foundItems = 0;
		newItems = 0;	
		setInfo("Inventory items loaded: " + g_dlInventory.rowCount(), "INFO");
		
		dlInventoryTmp.removeAll();
	}
}

function loadData2(source) {
	with (state) {		
		
		// Load data
		var dlInventoryTmp = null;
		newTags = new TagsCollection();
		
		var loader = new CSVLoader();
		loader.delimiter = ';';
		dlInventoryTmp = loader.loadUrl(source, true);
		java.lang.System.out.println("source: " + source);
		
		if (dlInventoryTmp == null) {
			setInfo("File not found.", "ERROR");
			return false;
		}
	
		if (dlInventoryTmp.rowCount() < 1) {
			setInfo("Data could not be loaded.", "ERROR");
			return false;
		}
		
		g_dlData.removeAll();
		
		for (var i=0; i<dlInventoryTmp.rowCount(); i++ ) {
			
			var addData = g_dlData.addBean();				
			addData.Location = "" + dlInventoryTmp[i][0];	
		}
	
		setInfo("Areas Found: " + g_dlData.rowCount(), "INFO");
		
		dlInventoryTmp.removeAll();		
		
		// prepare menulist
		// prepare subLocation dropdown and datalist
		var checkLoc = "";
		var lastLoc = "";
		mlLocation.removeAllItems();
		
		// headline in dropdown
		if (g_dlData.rowCount() > 0) {
			mlLocation.appendItems("Please Select Location", "null");
		}
			
		for (var i=0; i<g_dlData.rowCount(); i++) { 
			
			checkLoc = "" + g_dlData[i].Location;
						
			if (!checkLoc.equals(lastLoc)) {
				// new subLoc
				mlLocation.appendItems(checkLoc, checkLoc);
				lastLoc = "" + checkLoc;		
			}			
		}
		
	}
}

function checkServerAvailable() {
	with (state) {
		
		var settings = g_appCenter.queryElements("type=com:nofilis:app:settings;$select=id");
		// java.lang.System.out.println("settings: " + settings);
		
		var status = "" + settings;
		
		if (status != "[]" && status != "null") { // Server online
			g_connect = true;
			infoLabel.value = "Server Connected";
			
			resetInfo();
				
		} else { // Server offline
			g_connect = false;
			infoLabel.value = "Server Unreachable";
		}
	}
}


function selectItem() {
	
	iIndex = state.inventItems.selectedRow;
	return true;
}

function clearAllTags() {
	with (state) {

		var t = "";
        var idx = 0;
        var checkFound = "";

        for (var i = 0; i < g_dlObjects.rowCount(); i++) {

            checkFound = "" + g_dlObjects[i].found;

            if (checkFound.equals("1")) { // skip duplicate
                g_dlObjects[i].found = "0";
            	g_found--;
            }

        }
        g_current = 0;
        g_dlNewItems.removeAll();
		initF2();
    }
}


function readBtnDown() {
	with (state) {
		
		if (!readBtn.checked) {
			readBtn.label = "Read";
			stopRead();	
			
		} else {
			
			//initTagList();
			readBtn.label = "Stop";
			startRead();	
		}
	}
}

function startRead() {
	
	if (g_dlInventory == null) {
		setInfo("No data loaded.", "ERROR");
		return false;
		
	} 
	else {
		resetInfo();
	}

	setInfo("Reader active...", "BLINK");
	rfidReader.startRead();	
	
}

function stopRead() {
	with (state) {
	
		if (g_dlInventory == null) {
		return false;
		}
		
		var tags = rfidReader.stopReadCollection();
		addTags(tags);

		resetInfo();
	}
}

function readerGetTag() {
	
	var tags = rfidReader.getCurrentTagsCollection();
	addTags(tags);
	return false;
}

function addTags(tags) {
    with(state) {

        var t = "";
        var findTag = false;
        var findNewTag = false;
        var idx = 0;
        var checkFound = "";

        for (var i = 0; i < tags.length; i++) {
            t = "" + tags[i];

            if (t.equals("null")) {
                continue;
            }

            findTag = g_dlObjects.search("epc", t);

            if (!findTag) { // no item from inventory list detected

                findNewTag = g_dlNewItems.search("epc", t);

                if (findNewTag) { // already in new list
                    continue;

                } else {

                    var addTag = g_dlNewItems.addBean();
                    addTag.epc = "" + tags[i];
                    goodBeeper();
                }
            } else { // item belongs to inventory list

                // check duplicate
                idx = g_dlObjects.searchIndex("epc", t);
                checkFound = "" + g_dlObjects[idx].found;

                if (checkFound.equals("1")) { // skip duplicate
                    continue;
                }

                // update this inventory tag data
                g_dlObjects[idx].found = "1";
                g_found++;

            }
        }
		
		g_missing = g_total - g_found;
		g_new = g_dlNewItems.rowCount();
		g_current = g_found + g_new;

		itemsTotalCurrent.value = g_current.toString();
        itemsNew.value = g_new.toString()
        itemsMissing.value = g_missing.toString();
        itemsNewFound.value =g_found.toString();
		

    }
}

function newBtnDown() {
	with (state) {
		
		if (barcodeScanned == true) {
			
			if (!readBtn.checked) {
				readBtn.label = "Read";
				newStopRead();
				
			} else {
				
				readBtn.label = "Stop";
				newStartRead();	
			}		
		
		} else {
			
			if (!readBtn.checked) {
				readBtn.label = "Read";
				new_stopScan();	
				
			} else {
				
				readBtn.label = "Stop";
				new_startScan();	
			}		
		}
	}
}

function newStartRead() {
	resetInfo();

	rfidReader.startRead();
	setInfo("Reader active...", "BLINK");
}

function newStopRead() {
	
	resetInfo();
	var tags = rfidReader.stopReadCollection();
}

function newGetTag() {
	
	var tags = rfidReader.getCurrentTagsCollection();
	
	if (tags.length > 0) {
		
		addNewTags(tags);
	}
}

function addNewTags(tags) {
	with (state) {
		
		for (var i=0; i<tags.length; i++) {
			
			epcid.value = tags[0];
			readBtn.label = "Read";
			readBtn.checked = false;
			newStopRead();
		}
		setInfo(epcid.value, "INFO");
	}

}

function f2_sendData() {
	with (state) {

		var t = new String(Engine.currentTime());
		
		var checkEpc = "";
		var checkState = "0";

		for (var m=0; m<g_dlNewItems.rowCount(); m++) { // ["epc","found"];
						
			checkEpc = "" + g_dlNewItems[m].epc;	
			sendEvent(checkEpc,t);		
				
			
		}
			
			// delete
		g_dlObjects.removeAll();
		g_dlNewItems.removeAll();

		g_total = 0;
		g_missing = 0;
		g_found = 0;
			
		Engine.pause(1000);			
				
	}	
}

function isOdd(num) {
	return (num % 2) == 1;
}

function removeRows() {
	state.table1.removeAllRows()
}

function addTagInfo() {

	state.table1.removeAllRows();
	rowItemsTags = [];

	if(g_dlNewItems.rowCount() == 0) {
		state.table1.addRow("No Tags Available");
	}
	else {

		for (var m=0; m<g_dlNewItems.rowCount(); m++) { 				
		
		listItem = "" + g_dlNewItems[m].epc;
		
		if (rowItemsTags.indexOf(g_dlNewItems[m].epc)==-1)	
			state.table1.addRow(listItem);
			rowItemsTags.push(g_dlNewItems[m].epc)
			//if odd value set background different color
			if((m % 2) == 1) {
				state.table1.setRowBackground(m, "darkgray");
			}
			else {
				state.table1.setRowBackground(m, "white");
			}
		}

	}

}

function sendEvent(epc,t) {
	with (state) {
		
		markerIdHandheld = "NORDIC_" + g_areaId.toUpperCase();
		
		var e = new Event("com:nofilis:crosstalk:event:tag-observation");
		e.locationId = "" + markerIdHandheld;
		e.timestamp = "" + t;
		e.objectId = "" + epc;
	    
		var result = g_appCenter.sendEvent(e);

		if (g_appCenter.statusCode != 200) {
			g_err++;			
		}	
		else {
			setInfo(g_dlNewItems.rowCount() + " Tags Sent", "INFO");
		}
		
	}	
}
